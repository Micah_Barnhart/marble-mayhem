﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class LevelManager : MonoBehaviour
    {

        Text timerText;
        Text levelText;
        Text scoreText;
        int score;
        float timer;
        int level;
        int scoreToAdvanceLevel;
        private bool isLevelActive;
        private float timeTillLevelIsActive;
        int levelTime;
        public Text levelTextNotification;
        

        private void Start()
        {
            scoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
            timerText = GameObject.FindGameObjectWithTag("TimerText").GetComponent<Text>();
            levelText = GameObject.FindGameObjectWithTag("LevelText").GetComponent<Text>();
            score = 0;
            scoreText.text = score.ToString();
            level = 1;
            levelText.text = level.ToString();            
            timerText.text = timer.ToString();
            scoreToAdvanceLevel = 70;
            timeTillLevelIsActive = 5f;
            isLevelActive = true;
            levelTime = 60;
            timer = levelTime;
            levelTextNotification.GetComponent<Text>().enabled = false;
        }

        private void Update()
        {
            if(isLevelActive)
            {
                timer -= Time.deltaTime;
                int time = (int)timer;
                timerText.text = time.ToString();
                if (time <= 0)
                {
                    GameOver();
                }
                AdvanceLevel(score);
            }
            else
            {
                int time = (int)timer;
                timerText.text = time.ToString();
                timeTillLevelIsActive -= Time.deltaTime;
                if(timeTillLevelIsActive <= 0f)
                {
                    timeTillLevelIsActive = 5f;
                    isLevelActive = true;
                    levelTextNotification.enabled = false;
                }
            }
           
           
            
        }

        private void GameOver()
        {
            SceneManager.LoadScene(4);
        }

        public void AddPoints()
        {
            score += 10;
            scoreText.text = score.ToString();
        }

        void AdvanceLevel(int scoreValue)
        {
            if (scoreValue >= scoreToAdvanceLevel)
            {
                level++;
                isLevelActive = false;
                levelText.text = level.ToString();
                scoreToAdvanceLevel *= 2;
                levelTime *= 2;
                timer = levelTime;
                timerText.text = timer.ToString();
                levelTextNotification.enabled = true;
                levelTextNotification.text = "Level: " + level;
            }
        }

    }
}
