﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchMarble : MonoBehaviour {

    Color marbleColor;
    Color cupColor;
    LevelManager levelManager;

	// Use this for initialization
	void Start () {
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Marble")
        {
            cupColor = GetComponent<Transform>().FindChild("ParticleSystem").GetComponent<ParticleSystem>().startColor;
            marbleColor = other.gameObject.GetComponent<MeshRenderer>().material.color;
            
            if (cupColor == marbleColor)
            {
                levelManager.AddPoints();
                Destroy(other.gameObject);
                
            }
            else if (cupColor != marbleColor)
            {
                Destroy(other.gameObject);
                
                
            }

        }
        

    }
}
