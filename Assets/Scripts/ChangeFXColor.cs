﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFXColor : MonoBehaviour {

    ParticleSystem FX;
    float timeSinceColorChange = 0f;
    float timeBetweenColorChanges = 5f;
	// Use this for initialization
	void Start () {
        FX = GetComponentInChildren<ParticleSystem>();
        
	}
	
	// Update is called once per frame
	void Update () {
        timeSinceColorChange += Time.deltaTime;
        if (timeSinceColorChange >= timeBetweenColorChanges)
        {            
            timeSinceColorChange = 0f;            
            int randomColor = GetRandomColor();
            FX.startColor = GetNewColor(randomColor);
        }        
	}

   

    private Color GetNewColor(int randomColor)
    {
        if(randomColor == 1)
        {
            return new Color(1, 0, 0); // red
        }
        else if(randomColor == 2)
        {
            return new Color(0, 1, 0); // green
        }
        else if (randomColor == 3)
        {
            return new Color(0, 0, 1); // blue
        }
        else if (randomColor == 4)
        {
            return new Color(1, 1, 0); // yellow
        }
        else 
        {
            return new Color(1, 0, 1); // purple
        }
        

    }

    private int GetRandomColor()
    {
        return UnityEngine.Random.Range(1, 6);
    }
}
