﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarbleSpawner : MonoBehaviour {

    // Spawn Location Information
    Vector3 spawnPosition;
    Quaternion spawnRotation;
    bool isEmpty;
    public List<GameObject> marbles;


    // Use this for initialization
    void Start()
    {
        
        spawnPosition = transform.position;
        spawnRotation = transform.rotation;
        isEmpty = true;
        if(isEmpty)
        {
            SpawnMarble();
        }
    }

    public void SpawnMarble()
    {
        GameObject marble = GetMarble();       
        Instantiate(marble, spawnPosition, spawnRotation).transform.SetParent(transform);
        isEmpty = false;
    }

    private GameObject GetMarble()
    {
        int randIndex = UnityEngine.Random.Range(0, marbles.Count);
        return marbles[randIndex];
    }

    public void SetIsEmpty(bool emptyState)
    {
        isEmpty = emptyState;
    }
    public bool GetIsEmpty()
    {
        return isEmpty;
    }
    // Update is called once per frame
    void Update () {
        
        if(isEmpty)
        {
            SpawnMarble();
        }
	}

    
}
