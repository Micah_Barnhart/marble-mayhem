﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteOffScreenMarbles : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Marble")
        {
            Destroy(other.gameObject);
        }
    }
}
