﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameScore : MonoBehaviour
{
    Text scoreText;
    int score;
    private void Start()
    {
        scoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
        score = 0;
        scoreText.text = score.ToString();
    }

    public void AddPoints()
    {
        score += 10;
        scoreText.text = score.ToString();
    }
}