﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraneMovement : MonoBehaviour {

    Transform craneTransform;
    public float xSpeed;
    private bool goingDown;
    MarbleSpawner spawn1;
    MarbleSpawner spawn2;
    Transform child;
    Transform parentSpawnPoint;
    private bool needNewMarble;

    // Use this for initialization
    void Start () {
        craneTransform = GetComponent<Transform>();
        xSpeed = 1.0f;
        goingDown = true;
        spawn1 = GameObject.FindGameObjectWithTag("MarbleSpawnLeft").GetComponent<MarbleSpawner>();
        spawn2 = GameObject.FindGameObjectWithTag("MarbleSpawnRight").GetComponent<MarbleSpawner>();
        needNewMarble = false;
    }   

    // Update is called once per frame
    void Update () {       
       if(Input.GetMouseButton(0))
        {
            if (goingDown)
            {
                MoveCraneDown();
            }
            else if (!goingDown)
            {
                child.transform.SetParent(gameObject.transform);
                MoveCraneUp();
            }
        }
       if(Input.GetMouseButtonUp(0))
        {
            goingDown = true;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            transform.position -= new Vector3(xSpeed, 0, 0);
            CheckBoundary();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.position += new Vector3(xSpeed, 0, 0);
            CheckBoundary();
        }
        if(Input.GetMouseButtonDown(1))
        {
            Transform child = transform.GetChild(2);
            child.GetComponent<Rigidbody>().useGravity = true;
          //  child.GetComponent<Rigidbody>().isKinematic = true;
            child.transform.parent = null;
           
            
        }
    }

    

    private void MoveCraneUp()
    {       
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, 0.59f, 0), .07f);
        if(transform.position.y >= -1.75f && needNewMarble)
        {
            parentSpawnPoint.gameObject.GetComponent<MarbleSpawner>().SetIsEmpty(true);
            needNewMarble = false;
        }
        if(transform.position.y >= 0.58f)
        {
            goingDown = true;
        }      
    }
    private void MoveCraneDown()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, -2.6f, 0),.07f);
        if (transform.position.y <= -2.59f)
        {
            goingDown = false;
        }
    }
    private void CheckBoundary()
    {
        if(transform.position.x < -8f)
        {
            transform.position = new Vector3(-8f,transform.position.y,transform.position.z);
        }
        if (transform.position.x > 8f)
        {
            transform.position = new Vector3(8f, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {

        if (collision.tag == "Marble")
        {
            parentSpawnPoint = collision.transform.parent;
            child = collision.transform;
            needNewMarble = true;
            //collision.transform.SetParent(gameObject.transform);            
        }
    }


}
